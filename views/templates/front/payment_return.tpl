{extends "$layout"}

{block name="content"}
    <section class="card card-block">
        <h1>Order payment status</h1>

        {if isset($errors) && is_array($errors) }
            <div class="alert alert-danger">
                <ul style="margin-bottom: 0">
                    {foreach from=$errors item=error}
                        <li>{$error->detail}</li>
                    {/foreach}
                </ul>
            </div>
        {/if}

        <p><a href="/order?step=1">< Back to payment screen</a></p>
    </section>
{/block}
