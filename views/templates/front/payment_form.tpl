<!--
  These div elements are the placeholder elements that are replaced by the
  SqPaymentForm's iframes.
-->
<label>Card Number</label>
<div id="sq-card-number"></div>
<label>CVV</label>
<div id="sq-cvv"></div>
<label>Expiration Date</label>
<div id="sq-expiration-date"></div>
<label>Postal Code</label>
<div id="sq-postal-code"></div>

<div class="alert alert-warning" id="square-payment-error" style="display: none"></div>
<div class="alert alert-info" id="square-payment-info" style="display: none"></div>

<!--
  After the SqPaymentForm generates a card nonce, *this* form POSTs the generated
  card nonce to your application's server.

  You should replace the action attribute of the form with the path of
  the URL you want to POST the nonce to (for example, "/process-card")
-->
<form id="payment-form" novalidate action="{$action}" method="post">
    <input type="hidden" id="card-nonce" name="nonce" />
</form>

<script src="https://js.squareup.com/v2/paymentform"></script>
<script>
    var sqApplicationId = '{$square_app_id}';
</script>
