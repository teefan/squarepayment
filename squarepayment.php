<?php
require __DIR__ . '/vendor/autoload.php';

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_'))
{
    exit;
}

class SquarePayment extends PaymentModule
{
    protected $_html = '';
    protected $_postErrors = array();

    public function __construct()
    {
        $this->name = 'squarepayment';
        $this->tab = 'payments_gateways';
        $this->version = '0.1.6';
        $this->author = 'Long Tran';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->controllers = array('validation');
        $this->is_eu_compatible = 1;

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Square Payment');
        $this->description = $this->l('Simple Square payment module.');

        $this->templates_front_path = 'module:' . $this->name . '/views/templates/front/';

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('SQUARE_APP_ID'))
        {
            $this->warning = $this->l('No application ID provided');
        }

        if (!Configuration::get('SQUARE_ACCESS_TOKEN'))
        {
            $this->warning = $this->l('No application access token provided');
        }

        if (!Configuration::get('SQUARE_LOCATION_ID'))
        {
            $this->warning = $this->l('No Square location ID provided');
        }

        if (!count(Currency::checkPaymentCurrencies($this->id)))
        {
            $this->warning = $this->l('No currency has been set for this module.');
        }
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('paymentOptions') ||
            !$this->registerHook('paymentReturn') ||
            !$this->registerHook('displayHeader'))
        {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('SQUARE_APP_ID') ||
            !Configuration::deleteByName('SQUARE_ACCESS_TOKEN') ||
            !Configuration::deleteByName('SQUARE_LOCATION_ID') ||
            !Configuration::deleteByName('PERSONAL_ACCESS_TOKEN'))
        {
            return false;
        }

        return true;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active)
        {
            return false;
        }

        if (!$this->checkCurrency($params['cart']))
        {
            return false;
        }

        $payment_options = [
            $this->getEmbeddedPaymentOption()
        ];

        return $payment_options;
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->registerStylesheet(
            'module-squarepayment-style',
            'modules/' . $this->name . '/css/squarepayment.css',
            [
                'media' => 'all',
                'priority' => 200
            ]
        );

        // Smart cache combination doesn't position remote script correctly
        // $this->context->controller->registerJavascript(
        //     'module-squarepayment-sq-connect-script',
        //     'https://js.squareup.com/v2/paymentform',
        //     [
        //         'server' => 'remote',
        //         'position' => 'bottom',
        //         'priority' => 100
        //     ]
        // );

        $this->context->controller->registerJavascript(
            'module-squarepayment-script',
            'modules/' . $this->name . '/js/squarepayment.js',
            [
                'position' => 'bottom',
                'priority' => 200
            ]
        );
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module))
        {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency'])
                {
                    return true;
                }
            }
        }

        return false;
    }

    public function getEmbeddedPaymentOption()
    {
        $this->context->smarty->assign(array(
            'this_path_ssl' => Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/'
        ));

        $embeddedOption = new PaymentOption();
        $embeddedOption
            ->setCallToActionText($this->l('Pay by Credit Card'))
            ->setForm($this->generateForm())
            ->setAdditionalInformation(
                $this->context->smarty->fetch($this->templates_front_path . 'payment_infos.tpl')
            );

        return $embeddedOption;
    }

    protected function generateForm()
    {
        $square_app_id = Configuration::get('SQUARE_APP_ID');

        $this->context->smarty->assign([
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true),
            'square_app_id' => $square_app_id
        ]);

        return $this->context->smarty->fetch($this->templates_front_path . 'payment_form.tpl');
    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('submit-config'))
        {
            $updated = false;

            $square_app_id = (string)Tools::getValue('SQUARE_APP_ID');
            $square_access_token = (string)Tools::getValue('SQUARE_ACCESS_TOKEN');
            $square_location_id = (string)Tools::getValue('SQUARE_LOCATION_ID');

            if ($square_app_id && !empty($square_app_id))
            {
                Configuration::updateValue('SQUARE_APP_ID', $square_app_id);
                $updated = true;
            }

            if ($square_access_token && !empty($square_access_token))
            {
                Configuration::updateValue('SQUARE_ACCESS_TOKEN', $square_access_token);
                $updated = true;
            }

            if ($square_location_id && !empty($square_location_id))
            {
                Configuration::updateValue('SQUARE_LOCATION_ID', $square_location_id);
                $updated = true;
            }

            if ($updated)
            {
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        if (Tools::isSubmit('submit-location'))
        {
            $personal_access_token = (string)Tools::getValue('PERSONAL_ACCESS_TOKEN');

            if ($personal_access_token && !empty($personal_access_token))
            {
                $location_api = new \SquareConnect\Api\LocationApi();
                // $location_api->getApiClient()->getConfig()->setSSLVerification(false);
                $response = $location_api->listLocations($personal_access_token);
                $response_json = json_decode($response);

                $location_message = '<strong><p>Found following locations:</p></strong>';

                foreach ($response_json->locations as $location)
                {
                    $location_message .= '<p>ID: ' . $location->id . ' - Name: ' . $location->name . '</p>';
                }

                $output .= $this->displayConfirmation($location_message);
            }
        }

        return $output . $this->displayForm();
    }

    public function displayForm()
    {
        // Get default language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Simple Square Payment settings'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('SQUARE APPLICATION ID'),
                    'name' => 'SQUARE_APP_ID',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('SQUARE ACCESS TOKEN'),
                    'name' => 'SQUARE_ACCESS_TOKEN',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('SQUARE LOCATION ID'),
                    'name' => 'SQUARE_LOCATION_ID',
                    'required' => true
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'submit-config',
                'class' => 'btn btn-default pull-right'
            )
        );

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Simple Square Payment location tool'),
                'icon' => 'icon-wrench'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('GET LOCATIONS'),
                    'name' => 'PERSONAL_ACCESS_TOKEN',
                    'placeholder' => 'Input Access Token to get the location IDs from...'
                )
            ),
            'submit' => array(
                'title' => $this->l('Post'),
                'name' => 'submit-location',
                'class' => 'btn btn-default pull-right'
            )
        );

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        // $helper->submit_action = 'submit' . $this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        // Load current value
        $helper->fields_value['SQUARE_APP_ID'] = Configuration::get('SQUARE_APP_ID');
        $helper->fields_value['SQUARE_ACCESS_TOKEN'] = Configuration::get('SQUARE_ACCESS_TOKEN');
        $helper->fields_value['SQUARE_LOCATION_ID'] = Configuration::get('SQUARE_LOCATION_ID');
        $helper->fields_value['PERSONAL_ACCESS_TOKEN'] = '';

        return $helper->generateForm($fields_form);
    }
}
