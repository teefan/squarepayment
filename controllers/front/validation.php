<?php
require __DIR__ . '/../../vendor/autoload.php';

class SquarePaymentValidationModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        # Helps ensure this code has been reached via form submission
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
        {
            die($this->module->l('Invalid operation.', 'validation'));
        }

        error_log('SQUARE_PAYMENT POST: ' . print_r($_POST, true));

        $cart = $this->context->cart;
        if (!$this->module->active ||
            $cart->id_customer == 0 ||
            $cart->id_address_delivery == 0 ||
            $cart->id_address_invoice == 0)
        {
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module)
        {
            if ($module['name'] === 'squarepayment')
            {
                $authorized = true;
                break;
            }
        }

        if (!$authorized)
        {
            die($this->module->l('Simple Square payment method is not currently available.', 'validation'));
        }

        $this->context->smarty->assign([
            'params' => $_REQUEST,
        ]);

        $this->setTemplate($this->module->templates_front_path . 'payment_return.tpl');

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
        {
            Tools::redirect('index.php?controller=order&step=1');
        }

        $cart_id = $cart->id;
        $customer_id = $customer->id;

        # Fail if the card form didn't send a value for `nonce` to the server
        $nonce = $_POST['nonce'];
        if ($nonce === null || empty($nonce)) {
            error_log('SQUARE_PAYMENT_ERROR: empty nonce - CartID: ' . $cart_id . ' - CustomerID: ' . $customer_id);

            $this->context->smarty->assign('errors', [
                (object)[
                    'category' => 'PAYMENT_METHOD_ERROR',
                    'code' => 'CARD_INVALID',
                    'detail' => 'Card information is invalid.'
                ]
            ]);

            return;
        }

        $currency = $this->context->currency;
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $mailVars = array(
            '{square_payment_info}' => Configuration::get('SQUARE_PAYMENT_INFO')
        );

        // error_log('TOTAL: ' . print_r($total, true));

        $total_cents = bcmul($total, 100.0, 0); // $total * 100;

        $square_access_token = Configuration::get('SQUARE_ACCESS_TOKEN');
        $square_location_id = Configuration::get('SQUARE_LOCATION_ID');

        $transaction_api = new \SquareConnect\Api\TransactionApi();
        // $transaction_api->getApiClient()->getConfig()->setSSLVerification(false);

        $request_body = array (
            'reference_id' => 'customer-' . $customer->id . '---cart-' . $cart->id,
            'card_nonce' => $nonce,

            # Monetary amounts are specified in the smallest unit of the applicable currency.
            # This amount is in cents.
            'amount_money' => array (
                'amount' => (int)$total_cents,
                'currency' => 'USD'
            ),

            # Every payment you process with the SDK must have a unique idempotency key.
            # If you're unsure whether a particular payment succeeded, you can reattempt
            # it with the same idempotency key without worrying about double charging
            # the buyer.
            'idempotency_key' => uniqid('squarepayment', true)
        );

        # The SDK throws an exception if a Connect endpoint responds with anything besides
        # a 200-level HTTP code. This block catches any exceptions that occur from the request.
        try
        {
            $result = $transaction_api->charge($square_access_token, $square_location_id, $request_body);

            $transaction = $result->getTransaction();

            if (isset($transaction)) {
                $mailVars['transaction_id'] = $transaction->getId();
            }

            // error_log('PAYMENT TRANSACTION: ' . print_r($transaction, true));
        }
        catch (\SquareConnect\ApiException $e)
        {
            $errors = $e->getResponseBody()->errors;
            $this->context->smarty->assign('errors', $errors);

            // error_log('PAYMENT EXCEPTION: ' . print_r($errors, true));

            return;
        }

        $private_message = null;

        $this->module->validateOrder($cart->id, Configuration::get('PS_OS_PAYMENT'), $total, $this->module->displayName, $private_message, $mailVars, (int)$currency->id, false, $customer->secure_key);
        Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
    }
}
