// Square Payment script
// Initializes the payment form. See the documentation for descriptions of
// each of these parameters.
var isCardNumberValid = false;
var isCVVValid = false;
var isExpirationDateValid = false;
var isPostalCodeValid = false;

var initSquarePayment = function () {
    var sqPaymentForm = new SqPaymentForm({
        applicationId: sqApplicationId,
        inputClass: 'sq-input',
        inputStyles: [
            {
                fontSize: '16px'
            }
        ],
        cardNumber: {
            elementId: 'sq-card-number',
            placeholder: '•••• •••• •••• ••••'
        },
        cvv: {
            elementId: 'sq-cvv',
            placeholder: 'CVV'
        },
        expirationDate: {
            elementId: 'sq-expiration-date',
            placeholder: 'MM/YY'
        },
        postalCode: {
            elementId: 'sq-postal-code'
        },
        callbacks: {

            // Called when the SqPaymentForm completes a request to generate a card
            // nonce, even if the request failed because of an error.
            cardNonceResponseReceived: function(errors, nonce, cardData) {
                var $alertBox = $('#square-payment-error');
                var $infoBox  = $('#square-payment-info');

                if (errors) {
                    var error_messages = '';

                    // This logs all errors encountered during nonce generation to the
                    // Javascript console.
                    errors.forEach(function(error) {
                        // console.log(error);

                        error_messages += '<li>' + error.message + '</li>';
                    });

                    $alertBox.html('<ul style="margin-bottom: 0">' + error_messages + '</ul>');
                    $alertBox.show();

                    $infoBox.html('');
                    $infoBox.hide();
                } else {
                    // console.log('Card nonce received:', nonce);

                    document.getElementById('card-nonce').value = nonce;

                    $alertBox.html('');
                    $alertBox.hide();

                    $infoBox.html('Card is verified.');
                    $infoBox.show();
                }
            },

            unsupportedBrowserDetected: function() {
                // Fill in this callback to alert buyers when their browser is not supported.
                var $alertBox = $('#square-payment-error');
                $alertBox.html("We're sorry, your browser is not supported by Square Payment.");
                $alertBox.show();
            },

            // Fill in these cases to respond to various events that can occur while a
            // buyer is using the payment form.
            inputEventReceived: function(inputEvent) {
                switch (inputEvent.eventType) {
                    case 'focusClassAdded':
                        // Handle as desired
                        break;
                    case 'focusClassRemoved':
                        // Handle as desired
                        validateCard(inputEvent, sqPaymentForm);
                        break;
                    case 'errorClassAdded':
                        // Handle as desired
                        break;
                    case 'errorClassRemoved':
                        // Handle as desired
                        break;
                    case 'cardBrandChanged':
                        // Handle as desired
                        validateCard(inputEvent, sqPaymentForm);
                        break;
                    case 'postalCodeChanged':
                        // Handle as desired
                        validateCard(inputEvent, sqPaymentForm);
                        break;
                }
            },

            paymentFormLoaded: function() {
                // Fill in this callback to perform actions after the payment form is
                // done loading (such as setting the postal code field programmatically).
                // paymentForm.setPostalCode('94103');
            }
        }
    });
};

var validateCard = function(inputEvent, sqPaymentForm) {
    if (inputEvent.field === 'cardNumber' && inputEvent.currentState.isCompletelyValid) {
        isCardNumberValid = true;
    }
    if (inputEvent.field === 'cvv' && inputEvent.currentState.isCompletelyValid) {
        isCVVValid = true;
    }
    if (inputEvent.field === 'expirationDate' && inputEvent.currentState.isCompletelyValid) {
        isExpirationDateValid = true;
    }
    if (inputEvent.field === 'postalCode' && inputEvent.currentState.isCompletelyValid) {
        isPostalCodeValid = true;
    }

    if (isCardNumberValid && isCVVValid && isExpirationDateValid && isPostalCodeValid) {
        var $infoBox = $('#square-payment-info');
        $infoBox.html('Card is being verified...');
        $infoBox.show();

        sqPaymentForm.requestCardNonce();
    }
};

if (typeof(sqApplicationId) !== "undefined") {
    initSquarePayment();
}
